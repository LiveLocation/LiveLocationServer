#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Samtinel
#
# SPDX-License-Identifier: AGPL-3.0-only

from flask import Flask,request,abort,json,jsonify
import bcrypt

# replace this
SALT=b"replace me!" # generate with bcrypt.gensalt()
SECRET=b"replace me!" # generate with crypt.hashpw(PASSWORD, SALT)


app = Flask(__name__)

@app.before_first_request
def set_up():
    # the main variable to read and write location to
    global location
    # use a boolean value to check if cached_pw has been set. Setting it to None might work, but there is a JSON "null" value. TODO: investigate
    global password_known
    # cache pw after first time to avoid calculating the hash everytime
    global cached_pw
    password_known = False
    cached_pw = None
    location = {}
    location["lat"] = 53
    location["long"] = 8

@app.route("/", methods=["GET", "POST"])
def live_location_server():
    global location
    global cached_pw
    global password_known
    if request.method == 'POST':
        if not request.is_json or not is_request_valid(request):
            #DBG: print(request.get_json(force=True))
            abort(400)
        elif (password_known and request.json["secret"] == cached_pw) or bcrypt.hashpw(request.json["secret"].encode(), SALT) == SECRET:
            password_known = True
            cached_pw = request.json["secret"]
            try:
                location["long"] = float(request.json["long"])
                location["lat"] = float(request.json["lat"])
            except:
                abort(400)
            resp = jsonify(success=True)
            return resp
        else:
            # unauthorized/not authenticated
            abort(401)
    else:
        return location


# check all keys are in the request.json and are strings
def is_request_valid(request):
    KEYS = ["lat", "long", "secret"]
    for key in KEYS:
        if key not in request.json or type(request.json[key]) != type(""):
            return False
    return True
