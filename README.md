# LiveLocation Server
## Set-Up
Install the python packages mentioned in requirements.txt.  
Change salt and password hash in the main file.  
Run it with a WSGI server and behind a reverse proxy like nginx.  

## How does it work
### Update Location
Receives location via POST at /.  
Request looks like this:  
```{
    "lat": "53.12345",
    "long": "8.12345",
    "secret": "yoursecret"
}
```
Content-Type must be set to json.  

### Get Location
Gets location via POST to /.  
Returns:  
```{  
    "lat": "53.12345",
    "long": "8.12345"
}
```

## How to make use of it?
The ""protocol"" (see "how does it work") is very easy and all of the LiveLocation programs are dead simple to set-up and change.  
For the use-case of location broadcasting, you'll need three parts:
 - the server (you're looking at its repo :))
 - the publisher of the location
 - the application to show the location
The publisher is in https://codeberg/LiveLocation/ at the LiveLocationPublisher-\* repos. Currently, only a web publisher exists, but I am working on an android app to save battery. To show the location, you can use the very simple web page from the LiveLocationViewer.

## Things to improve
Maybe consider using http auth instead from the web server or something to get rid of the code in this server and simplify it even more (and avoid pitfalls).  
Rewrite in a language like Go to get rid of overhead.
